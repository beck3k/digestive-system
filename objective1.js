const elements = {
    ingestion: {
        "title": "Ingestion",
        "slug": "ingestion"
    },
    propulsion: {
        "title": "Propulsion",
        "slug": "propulsion"
    },
    breakdown: {
        "title": "Mechanical breakdown",
        "slug": "breakdown"
    },
    digestion: {
        "title": "Digestion",
        "slug": "digestion"
    },
    absorption: {
        "title": "Absoption",
        "slug": "absorption"
    },
    defecation: {
        "title": "Defecation",
        "slug": "defecation"
    }
};

var selectedElement;

function clearSelectionPanel() {
    $("#selection-panel").children().each((key, id) => {
        $(id).removeClass("active");
    });
}

function drawSelection() {
    $("#selected-element-info").text(elements[selectedElement].title);
}

function bindSelectors() {
    $("#selection-panel").children().each((key, obj) => {
        try {
            const slug = $(obj).data('selection');
            const elementKey = Object.keys(elements).find(_obj => elements[_obj].slug == slug);
            $(obj).click(() => {
                clearSelectionPanel();
                $(obj).addClass("active");
                selectedElement = elementKey;
                drawSelection();
            })
        } catch(e) {
            // console.log(e);
        }
    });
}

bindSelectors();